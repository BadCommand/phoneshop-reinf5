package phoneshop;

public class PhoneShop {

	String owner;
	int amountPhones;
	int amountCases;

	public PhoneShop(String owner) {
		this.owner = owner;
	}

	public String getOwner() {

		return owner;
	}

	public int getPhoneStock() {

		return amountPhones;
	}

	public int getPhonecaseStock() {
		return this.amountCases;
	}

	public void fillStockCases(int number) {
		amountCases += number;
	}

	public void fillStockPhones(int number) {
		amountPhones += number;
	}

	public String phoneSold() {

		if (amountPhones >= 1) {
			amountPhones -= 1;
			return "OK";
		} else {
			return "NOK";
		}
	}

	public String caseSold() {

		if (amountCases >= 1) {
			this.amountCases -= 1;
			return "OK";
		} else {
			return "NOK";
		}
	}
}
